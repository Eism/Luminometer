package com.example.ae3.lumenometer.mainactivityfragment;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ae3.lumenometer.DataProcessing;
import com.example.ae3.lumenometer.MainActivity;
import com.example.ae3.lumenometer.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Vector;

/**
 * Created by IIsmailzada on 27.04.2016.
 */
public class MainFragment4 extends Fragment implements View.OnClickListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public DataProcessing dataProcessing=new DataProcessing();

    public MainFragment4() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MainFragment4 newInstance(int sectionNumber) {
        MainFragment4 fragment = new MainFragment4();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_4, null);
        final GraphView graph = (GraphView) rootView.findViewById(R.id.graph);
        final MainActivity actOne = (MainActivity) getActivity();
        final TextView textView = (TextView) rootView.findViewById(R.id.textViewPack);
        final Button button =
                (Button) rootView.findViewById(R.id.buttonResult);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (actOne.mode == 1) {
                    Vector<Integer> pack = actOne.packag.elementAt(actOne.packag.size() - 1);

                    double delta = 0.01;
                    int size = pack.size();

                    dataProcessing.setX(delta, size);
                    dataProcessing.setY(pack);

                    Double resultData;
                    resultData = dataProcessing.aproxFlow();

                    DataPoint[] points = new DataPoint[size];

                    for (int i = 0; i < size; i++) {
                        points[i] = new DataPoint(delta, resultData);
                        delta += delta;
                    }

                    LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(points);

                    graph.addSeries(series);
                    graph.getViewport().setScalable(true);
                    graph.getViewport().setScrollable(true);

                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getViewport().setMinX(0.01);
                    graph.getViewport().setMaxX(delta);
                    graph.getViewport().setMinY(0.01);
                    graph.getViewport().setMaxY(resultData+100);

                    // создаем объект для данных
                    ContentValues cv = new ContentValues();

                    // подключаемся к БД
                    SQLiteDatabase db = actOne.dbHelper.getWritableDatabase();

                    Cursor c = db.query(actOne.DATABASE_NAME,
                            new String[]{"name","flowmode"}, "name = ?", new String[]{actOne.Sample}, null, null, null);

                    if (c.getCount()>0){
                        int flowmode=c.getColumnIndex("flowmode");

                        Integer sampleResultData=c.getInt(flowmode);

                        textView.setText(resultData.toString()+ " - " + sampleResultData.toString());
                    }
                }
                if (actOne.mode == 2) {
                    Vector<Vector<Integer>> pack = new Vector<Vector<Integer>>();
                    int impulseAmount = actOne.impulseAmount;
                    int packegeSize = actOne.packag.size();
                    for (int i = packegeSize - impulseAmount; i < packegeSize; i++) {
                        pack.add(actOne.packag.elementAt(i));
                    }
                    textView.setText(pack.toString());
                }
            }
        });

        final Button button1 =
                (Button) rootView.findViewById(R.id.buttonAgain);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, new MainFragment2(), "Frag2");
                transaction.commit();
            }
        });
        return rootView;
    }


    @Override
    public void onClick(View v) {

    }
}

