package com.example.ae3.lumenometer.mainactivityfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ae3.lumenometer.MainActivity;
import com.example.ae3.lumenometer.R;

/**
 * Created by IIsmailzada on 27.04.2016.
 */
public class MainFragment2 extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public MainFragment2() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MainFragment2 newInstance(int sectionNumber) {
        MainFragment2 fragment = new MainFragment2();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_2, null);

        final MainActivity actOne = (MainActivity) getActivity(); // ссылка на главное активити
        final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        // кнопка назад
        final Button button =
                (Button) rootView.findViewById(R.id.buttonBackBT);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                actOne.fragment.destroyConnect();
                transaction.replace(R.id.container, new MainFragment1(), "Frag1");
                transaction.commit();
            }
        });

        // кнопка выбора режима "поток"
        final Button button1=(Button) rootView.findViewById(R.id.buttonFlow);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actOne.mode=1;
                // отправка команды "поток"
                actOne.fragment.setMessage(new byte[]{0x0B});
                transaction.replace(R.id.container,new MainFragment3(),"Frag3");
                transaction.commit();
            }
        });

        // кнопка выбора режима "импульс"
        final Button button2=(Button) rootView.findViewById(R.id.buttonImpulse);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actOne.mode=2;
                // отправка команды "импульс"
                actOne.fragment.setMessage(new byte[]{0x0C});
                transaction.replace(R.id.container,new MainFragment3(),"Frag3");
                transaction.commit();
            }
        });
        return rootView;
    }
}

