package com.example.ae3.lumenometer.mainactivityfragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ae3.lumenometer.MainActivity;
import com.example.ae3.lumenometer.R;

/**
 * Created by IIsmailzada on 27.04.2016.
 */
public class MainFragment1 extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    MainActivity actOne; // указатель на главное активити

    ProgressConnectBT progressConnectBT; // поток ожидания окончания сиоединения с Bluetooth
    ProgressDialog progresDialog;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            switch (message.what){
                case 0:
                    StopProgressDialog();
                    break;
                case 1:
                    StartProgressDialog();
                    break;
                case 2:
                    StopProgressDialog();
                    StartFragment2();
                    break;
                default:
                    break;
            }
        }
    };

    public MainFragment1(){
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MainFragment1 newInstance(int sectionNumber) {
        MainFragment1 fragment = new MainFragment1();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_1, null);
        actOne = (MainActivity) getActivity();

        progressConnectBT=new ProgressConnectBT();
        progressConnectBT.start();

        // кнопка включения BT
        final Button button =
                (Button) rootView.findViewById(R.id.buttonBluetooth);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // выбор устройства и соединение с ним
                actOne.fragment.connectBt();
            }
        });
        return rootView;
    }


    class ProgressConnectBT extends Thread{
        boolean isWork=true;
        boolean isFirstVisit=true;
        ProgressConnectBT(){
            super("Thread");
        }

        public void run(){
            while(true){
                // нет соединения с BT
                if (actOne.fragment.getStatusBT()==0){
                    Message msg=mHandler.obtainMessage(0);
                    msg.sendToTarget();
                    isFirstVisit=true;
                }
                // на стадии соединения с BT
                if (actOne.fragment.getStatusBT()==1){
                    if (isFirstVisit){
                        isFirstVisit=false;
                        Message msg=mHandler.obtainMessage(1);
                        msg.sendToTarget();
                    }
                }
                // успешное соединение с BT
                if (actOne.fragment.getStatusBT()==2){
                    if (isWork){
                        isWork=false;
                        Message msg=mHandler.obtainMessage(2);
                        msg.sendToTarget();
                        break;
                    }
                }

            }
        }

    }

    private void StopProgressDialog(){
        if (progresDialog!=null)
            if (progresDialog.isShowing()) {
                progresDialog.dismiss();
                progresDialog = null;
            }
    }
    private void StartProgressDialog(){
        if (progresDialog==null) {
            progresDialog = new ProgressDialog(getContext());
            progresDialog.setMessage("Подключение...");
            progresDialog.setIndeterminate(true);
            progresDialog.setCancelable(false);
        }
        progresDialog.show();
    }
    private void StartFragment2() {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new MainFragment2(), "Frag2");
        transaction.commit();
    }

}

