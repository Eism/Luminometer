package com.example.ae3.lumenometer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.ae3.lumenometer.bluetoothchat.BluetoothChatFragment;
import com.example.ae3.lumenometer.mainactivityfragment.MainFragment1;

import java.util.Vector;
import java.util.zip.CRC32;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final byte GET_DATE = 0x0A; // команда начала получения пакетов

    private static final int CRC_YES = 0x0D; // команда успешного CRC
    private static final int CRC_NOT = 0x0E; // команда неуспешного CRC

    public String Sample ="";
    public int mode = 0;// 1-потоковый 2-импульсный
    public volatile int impulseAmount = 5; // кол-во пакетов в импульсном режиме
    public volatile int recievedPackageNumber = 0; // кол-во полученных пакетов в импульсном режиме
    public volatile int packProgress = 0; // стадия получения пакета
    public volatile int finishGettingPackage = 0; // окончание получения пакетов

    private static final String TAG = "Packege";

    public BluetoothChatFragment fragment; // класс Bluetooth

    public Thread dataAnalyses; // поток-обработчик пакетов

    final public Vector<Vector<Integer>> packag = new Vector<Vector<Integer>>(); // контейнер полученных успешных пакетов

    FrameLayout container; // для фрагментов
    FragmentManager myFragmentManager;

    public DBHelper dbHelper;
    public static final String DATABASE_NAME = "DB.db";
    private static final int DATABASE_VERSION = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragment = new BluetoothChatFragment();
        dataAnalyses = new Thread(funThreadDataProcessing);
        dataAnalyses.start();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.sample_content_fragment, fragment);
        transaction.commit();


        dbHelper = new DBHelper(this);
        SQLiteDatabase db;
        try {
            db = dbHelper.getWritableDatabase();
        }
        catch (SQLiteException ex){
            db = dbHelper.getReadableDatabase();
        }

        // вызов fragment`ов
        container = (FrameLayout) findViewById(R.id.container);
        myFragmentManager = getSupportFragmentManager();


        FragmentTransaction fragmentTransaction = myFragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.container, new MainFragment1(), "Fragm1");
        fragmentTransaction.commit();
    }

    private Runnable funThreadDataProcessing = new Runnable() {
        @Override
        public void run() {
            while (true) {
                Byte command = fragment.getCommand();
                if (command != 0)
                    switch (command) {
                        case GET_DATE:

                            // ждем для получения пакета
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            Vector<Integer> temppackage = new Vector<Integer>(); // пакет

                            Integer size = mergeIntoOne(fragment.getCommand(), fragment.getCommand()); // длина пакета

                            packProgress = 10;

                            byte[] data = new byte[size * 2]; // пакет в двухбайтовом виде
                            for (int i = 0; i < size * 2; i++) {
                                data[i] = fragment.getCommand();
                            }

                            packProgress = 50;

                            // переводим в однобайтовый
                            for (int i = 0; i < size * 2; i += 2) {
                                int k = mergeIntoOne(data[i], data[i + 1]);
                                temppackage.add(k);
                            }

                            packProgress = 70;

                            // получаем CRC
                            byte a = fragment.getCommand();
                            byte b = fragment.getCommand();
                            byte c = fragment.getCommand();
                            byte d = fragment.getCommand();

                            long CRC = unsignedToInt((unsignedToBytes(d) << 24) +
                                    (unsignedToBytes(c) << 16) +
                                    (unsignedToBytes(b) << 8) +
                                    (unsignedToBytes(a)));

                            // просчитываем CRC для полученного пакета
                            CRC32 myCRC = new CRC32();
                            myCRC.update(data);
                            long resultCRC = myCRC.getValue();

                            // очищаем контейнер пакетов в классе Bluetooth
                            fragment.clearList();

                            // сравниваем CRC
                            if (CRC == resultCRC) {

                                // отправляем успешно
                                fragment.setMessage(new byte[]{CRC_YES});
                                // запоминаем пакет
                                packag.add(temppackage);

                                // говорим об окончании получения пакетов
                                if (mode == 1) {
                                    packProgress = 100;
                                    finishGettingPackage = 1;
                                } else {
                                    if (mode == 2) {
                                        if ((recievedPackageNumber + 1) == impulseAmount) {
                                            finishGettingPackage = 1;
                                            recievedPackageNumber = 0;
                                        } else {
                                            recievedPackageNumber++;
                                        }
                                    }
                                }
                            } else {
                                // отправляем неуспешно
                                fragment.setMessage(new byte[]{CRC_NOT});
                            }
                            break;
                        case 0:
                            break;
                        default:
                            break;
                    }
            }
        }
    };

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int count = getSupportFragmentManager().getBackStackEntryCount();

            if (count == 0) {
                super.onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    @Override
    public void onDestroy() {
        moveTaskToBack(true);

        super.onDestroy();

        System.runFinalizersOnExit(true);
        System.runFinalization();
        System.exit(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public int mergeIntoOne(Byte a, Byte b) {
        return ((unsignedToBytes(b) << 8) + unsignedToBytes(a));
    }

    public static int unsignedToBytes(byte b) {
        return b & 0xFF;
    }

    public static long unsignedToInt(int x) {
        return x & 0xffffffffL;
    }

    public class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("DB", "--- onCreate database ---");
            // создаем таблицу с полями
            db.execSQL("create table mytable ("
                    + "id integer primary key autoincrement,"
                    + "name text,"
                    + "flowmode integer" +
                    "impulsemode1 integer" +
                    "impulsemode2 integer" +
                    "impulsemode3 integer" +
                    "impulsemode4 integer" +
                    "impulsemode5 integer" +
                    "impulsemode6 integer" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
