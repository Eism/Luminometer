package com.example.ae3.lumenometer;

import java.util.Vector;

import static java.lang.Math.*;

/**
 * Created by IIsmailzada on 28.03.2016.
 */
public class DataProcessing {

    private double[] x;
    private int[] y;
    private int length;

    public void setX(double delta, int length_){
        x=new double[length_+1];
        for(int i=0;i<length_;i++){
            x[i]=delta;
            delta+=delta;
        }
        length=length_;
    }

    public void setY(Vector<Integer> y_){
        y=new int[length+1];
        for(int i=0;i<length;i++){
            y[i]=y_.elementAt(i);
        }
    }

    public void aproxImpulse(double[] data){

        Double sumX = 0.0;
        Double sumY = 0.0;
        Double sumSqrX = 0.0;
        Double sumXY = 0.0;

        for(int i=0;i<length;i++)
        {
            sumX = sumX + x[i];
            sumY = sumY + log(abs(y[i]));
            sumSqrX = sumSqrX + x[i]*x[i];
            sumXY = sumXY + x[i]*log(abs(y[i]));
        }
        double b1 = (length*sumXY-sumX*sumY)/(length*sumSqrX-sumX*sumX);
        if(length*sumSqrX==sumX*sumX) b1=1;

        Double a1 = (sumY-b1*sumX)/length;
        Double c1 = b1;
        Double c2 = exp(a1);

        for(int i=0;i<length;i++)
        {
            data[i]=c2*exp(c1*x[i]);
        }
    }

    public double aproxFlow(){
        double resultFlow=0;
        for(int i=0;i<length;i++)
        {
            resultFlow+=y[i];
        }
        return resultFlow/=((double)length);
    }

    public DataProcessing(){

    }


}
