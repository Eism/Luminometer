package com.example.ae3.lumenometer.mainactivityfragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ae3.lumenometer.MainActivity;
import com.example.ae3.lumenometer.R;


/**
 * Created by IIsmailzada on 27.04.2016.
 */
public class MainFragment3 extends Fragment implements View.OnClickListener {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    MainActivity actOne; //ссылка на главное активити
    WaitingForGettingData waitingForGettingData; // поток ожидания получения пакетов
    private ProgressDialog progresDialog;

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    StartFragment4();
                    break;
                default:
                    break;
            }
        }
    };

    public MainFragment3() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MainFragment3 newInstance(int sectionNumber) {
        MainFragment3 fragment = new MainFragment3();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_3, null);

        actOne = (MainActivity) getActivity();
        final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        waitingForGettingData = new WaitingForGettingData();
        waitingForGettingData.start();

        final Button button =
                (Button) rootView.findViewById(R.id.buttonBackMode);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                transaction.replace(R.id.container, new MainFragment2(), "Frag2");
                transaction.commit();
            }
        });
        final Button button1 = (Button) rootView.findViewById(R.id.buttonStart);
        button1.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonStart:
                final MainActivity actOne = (MainActivity) getActivity();

                // отправка начала передачи пакетов
                actOne.fragment.setMessage(new byte[]{0x0A});

                if (progresDialog == null) {
                    progresDialog = new ProgressDialog(getContext());
                    progresDialog.setMessage("Получение данных. Пожалуйста подождите.");
                    progresDialog.setIndeterminate(true);
                    progresDialog.setCancelable(false);
                }
                progresDialog.show();

                break;
            case R.id.buttonBackMode:
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, new MainFragment2(), "Frag2");
                transaction.commit();
                break;
            default:
                break;
        }
    }

    private void StartFragment4() {
        if (progresDialog != null)
            if (progresDialog.isShowing()) {
                progresDialog.dismiss();
                progresDialog = null;
            }
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new MainFragment4(), "Frag4");
        transaction.commit();
    }

    class WaitingForGettingData extends Thread {
        boolean isWork;
        boolean isFirstVisit;

        WaitingForGettingData() {
            super("Thread");
            isWork = true;
            isFirstVisit = true;
        }

        public void run() {
            while (true) {
                if (actOne.finishGettingPackage==1){
                    if (isFirstVisit) {
                        Message msg = mHandler.obtainMessage(1, 0);
                        msg.sendToTarget();
                        actOne.finishGettingPackage=0;
                        isFirstVisit=false;
                    }
                }
            }
        }
    }
}
