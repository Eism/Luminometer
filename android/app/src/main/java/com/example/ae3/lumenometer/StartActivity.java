package com.example.ae3.lumenometer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.ae3.lumenometer.firstvisitactivity.FirstVisitActivity;

public class StartActivity extends AppCompatActivity  {

    // имя файла настройки
    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_COUNTER = "counter";
    private SharedPreferences mSettings;

    boolean firstVisit=true; // проверка первого посещения приложения

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        final ImageView img= (ImageView) findViewById(R.id.image);
        img.setImageResource(R.drawable.im_start_image);

        // задержка на две секунды для показа картинки
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (firstVisit){
                    // вызов приветственной Activity
                    Intent intent = new Intent(getApplicationContext(),FirstVisitActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    firstVisit=false;
                }else{
                    // вызов главного окна приложения
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        }, 2000);

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }

    @Override
    protected void onResume(){
        super.onResume();

        if (mSettings.contains(APP_PREFERENCES_COUNTER)) {
            // Получаем значение firstVisit из настроек
            firstVisit = mSettings.getBoolean(APP_PREFERENCES_COUNTER, true);
        }
    }

    @Override
    protected void onPause(){
        super.onPause();

        // сохраняем значение firstVisit в файл
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(APP_PREFERENCES_COUNTER, firstVisit);
        editor.apply();
    }

}
