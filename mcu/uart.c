#include "commands.h"
#include "uart.h"

void UARTGPIOConfiguration()
{
  	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure USART1 Tx (PA.09) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure USART1 Rx (PA.10) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}


void UARTBufferInit()
{
	UARTBuffer.handler = UARTBuffer.com;
	UARTBuffer.interrupter = UARTBuffer.com;
	UARTBuffer.commandRecieved = 0;
}

void UARTInit()
{
	UARTGPIOConfiguration();
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2ENR_AFIOEN , ENABLE);
	USART_InitTypeDef USART_InitStructure;

	/* USART1 configuration ------------------------------------------------------*/
	USART_InitStructure.USART_BaudRate = 9600;        // Baud Rate
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART1, &USART_InitStructure);
	USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
	UARTBufferInit();
	NVIC_EnableIRQ(USART1_IRQn);

	USART_Cmd(USART1, ENABLE);
}

void UARTSend(uc8* pucBuffer, u16 pucLength)
{
	while(pucLength--){
        USART_SendData(USART1, (uint16_t) *pucBuffer++);
        while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
	}
}

void USART1_IRQHandler()
{
	if (USART_GetFlagStatus(USART1,USART_FLAG_RXNE)){
		UARTBuffer.commandRecieved++;
		if (UARTBuffer.interrupter == UARTBuffer.com+1000) {
			UARTBuffer.interrupter = UARTBuffer.com;
		}
		if (UARTBuffer.interrupter + 1 == UARTBuffer.handler) {
			u8 message[] = {COMMAND_REFUSE,COMMAND_END}; //����� � ����������
			UARTSend(message,2);
		}
		*UARTBuffer.interrupter++ = USART_ReceiveData(USART1);
	}
	USART_ClearITPendingBit(USART1,USART_IT_RXNE);
}
