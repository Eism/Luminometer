#ifndef COMMANDS_H
#define COMMANDS_H

#include "stm32f10x_rcc.h"
#include "stdlib.h"

#define COMMAND_REFUSE ((u8)0xff)
#define COMMAND_END ((u8)0x00)
#define COMMAND_SEND_DATA ((u8)0x0A)
#define COMMAND_FLOW_MODE ((u8)0x0B)
#define COMMAND_IMPULSE_MODE ((u8)0x0C)
#define COMMAND_SUCCESS ((u8)0x0D)
#define COMMAND_FAIL ((u8)0x0E)

typedef struct{
	u8 Mode;
	u8 ImpulseNum;
}ConfigStruct;

ConfigStruct Cfg;

#endif
