#include "stdlib.h"
#include "string.h"
#include "packet.h"
#include "commands.h"
#include "stm32f10x_crc.h"
#include "stm32f10x_rcc.h"

u32 reverse_32(u32 data)
{
        return __RBIT(data);
};

u32 crc32_ether(u8 *buf, u32 len, u32 clear)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
        u32 *p = (u32*) buf;
        u32 crc, crc_reg;
        if(clear) CRC_ResetDR();
        while(len >= 4) {
                crc_reg = CRC_CalcCRC(reverse_32(*p++));
                len -= 4;
        }
        crc = reverse_32(crc_reg);
        if(len) {
                CRC_CalcCRC(crc_reg);
                switch(len) {
                        case 1:
                        crc_reg = CRC_CalcCRC(reverse_32((*p & 0xFF) ^ crc) >> 24);
                        crc = ( crc >> 8 ) ^ reverse_32(crc_reg);
                        break;
                        case 2:
                        crc_reg = CRC_CalcCRC(reverse_32((*p & 0xFFFF) ^ crc) >> 16);
                        crc = ( crc >> 16 ) ^ reverse_32(crc_reg);
                        break;
                        case 3:
                        crc_reg = CRC_CalcCRC(reverse_32((*p & 0xFFFFFF) ^ crc) >> 8);
                        crc = ( crc >> 24 ) ^ reverse_32(crc_reg);
                        break;
                }
        }
        return ~crc;
        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, DISABLE);
}

TDataPackage formingCommandPackage(u8 command)
{
	TDataPackage DataPackage;
	DataPackage.Size = 2;
	DataPackage.Package = (u8*)malloc(2*sizeof(u8));
	DataPackage.Package[0] = command;
	DataPackage.Package[1] = COMMAND_END;
	return DataPackage;
}

TDataPackage formingPackage(u8 command, u16* data, u16 dataSize)
{
	TDataPackage DataPackage;
	DataPackage.Size = 2*sizeof(u8)+dataSize*sizeof(u16)+sizeof(u32)+2;
	DataPackage.Package = (u8*)malloc(DataPackage.Size);
	for(int i=0;i<DataPackage.Size;i++)DataPackage.Package[i]=0;
	DataPackage.Package[0] = command;
	memcpy(DataPackage.Package+1,&dataSize,2);
//	{
//		DataPackage.Package[1] = (u8)((dataSize&0xFF00)>>8);
//		DataPackage.Package[2] = (u8)(dataSize&0x00FF);
//	}
	memcpy(DataPackage.Package+3,data,dataSize*2);
//	for(int i=0; i<dataSize; i++)
//	{
//		DataPackage.Package[3+i*2]=(u8)((data[i]&0xFF00)>>8);
//		DataPackage.Package[4+i*2]=(u8)(data[i]&0x00FF);
//	}
	u32 temp = crc32_ether((u8*)data,dataSize*2,1);
	memcpy(DataPackage.Package+DataPackage.Size-5,&temp,4);
//	{
//		DataPackage.Package[DataPackage.Size-2]=(u8)((temp&0xFF000000)>>24);
//		DataPackage.Package[DataPackage.Size-3]=(u8)((temp&0x00FF0000)>>16);
//		DataPackage.Package[DataPackage.Size-4]=(u8)((temp&0x0000FF00)>>8);
//		DataPackage.Package[DataPackage.Size-5]=(u8)(temp&0x000000FF);
//	}
	DataPackage.Package[DataPackage.Size-1] = COMMAND_END;
	return DataPackage;
}
