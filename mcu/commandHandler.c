#include "commandHandler.h"
#include "commands.h"
#include "stdlib.h"

void commandHandler()
{
	if (UARTBuffer.handler!=UARTBuffer.interrupter)
	{
		if (UARTBuffer.handler == UARTBuffer.com+1000) {
			UARTBuffer.handler = UARTBuffer.com;
		}
		switch (*UARTBuffer.handler) {
			case COMMAND_SEND_DATA:
			{
				UARTBuffer.handler++;
				UARTBuffer.commandRecieved--;
				for (u8 i=0;i<Cfg.ImpulseNum;i++){
					ResetData();
					StartConversion();
					while (ADCstr.f==0);
					TDataPackage Package = formingPackage(0x0A,ADCstr.ADCdata/*mes*/,100);
					UARTSend(Package.Package,Package.Size);
					while(*UARTBuffer.handler!=COMMAND_SUCCESS){
						if (*UARTBuffer.handler == COMMAND_FAIL && UARTBuffer.commandRecieved>0){
							UARTBuffer.handler++;
							UARTBuffer.commandRecieved--;
							UARTSend(Package.Package,Package.Size);
						}
						//Sleep
					}
					free(Package.Package);
					UARTBuffer.handler++;
				}
				ResetData();
				break;
			}
/*			case 'a':
			{
				ResetData();
				StartConversion();
				while (ADCstr.f==0);
				TDataPackage Package = formingPackage(0x0A,ADCstr.ADCdata,100);
				UARTSend(Package.Package,Package.Size);
				UARTBuffer.handler++;
				free(Package.Package);
				ResetData();
				break;
			}*/
			case COMMAND_FLOW_MODE:
			{
				Cfg.Mode=0;
				Cfg.ImpulseNum=1;
				UARTBuffer.handler++;
				UARTBuffer.commandRecieved--;
				break;
			}
			case COMMAND_IMPULSE_MODE:
			{
				Cfg.Mode=1;
				Cfg.ImpulseNum=5;
				UARTBuffer.handler++;
				UARTBuffer.commandRecieved--;
				/*�������� ���������� ���������*/
				break;
			}
			default:
			{
				break;
			}
		}
	}
}
