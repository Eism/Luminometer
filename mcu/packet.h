#ifndef PACKET_H
#define PACKET_H

#include "stm32f10x.h"

typedef struct
{
	u32 Size;
	u8* Package;
}TDataPackage;

TDataPackage formingCommandPackage(u8 command);
TDataPackage formingPackage(u8 command, u16* data, u16 dataSize);

#endif
