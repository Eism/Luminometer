#include "adc.h"

void ADCInit(){

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2ENR_AFIOEN , ENABLE);

	ADC_InitTypeDef ADC_InitStructure;
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	ADC_Cmd(ADC1, ENABLE);

	ADCstr.ADCdata = (u16*)malloc(100*sizeof(u16));

	ResetData();

	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_55Cycles5);

	ADC_ResetCalibration(ADC1);
	while (ADC_GetResetCalibrationStatus(ADC1));
	ADC_StartCalibration(ADC1);
	while (ADC_GetCalibrationStatus(ADC1));

	NVIC_EnableIRQ(ADC1_2_IRQn);
	ADC1->CR1 |= ADC_CR1_EOCIE;
}

void StartConversion(){
	ADC_SoftwareStartConvCmd(ADC1,ENABLE);
}

void StopConversion(){
	ADC_SoftwareStartConvCmd(ADC1,DISABLE);
}

void ResetData(){
	memset(ADCstr.ADCdata,0,100);
	ADCstr.i=0;
	ADCstr.f=0;
}

u16* GetData(){
	return ADCstr.ADCdata;
}

void ADC1_2_IRQHandler(){
	if (ADCstr.i<100){
		while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
		ADCstr.ADCdata[ADCstr.i]=ADC_GetConversionValue(ADC1);
		ADCstr.i++;
	}
	if(ADCstr.i>=100){
		ADCstr.f=1;
		StopConversion();
	}
	ADC_ClearFlag(ADC1,ADC_FLAG_EOC);
}
