#ifndef UART_H
#define UART_H

#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"

void UARTInit();
void UARTSend(uc8* pucBuffer, u16 pucLength);

typedef struct{
	u8 com[1000];
	u8* handler;
	u8* interrupter;
	u8 commandRecieved;
}TUARTBuffer;
TUARTBuffer UARTBuffer;


#endif
