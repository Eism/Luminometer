#ifndef ADC_H
#define ADC_H

#include "stm32f10x_adc.h"
#include "stm32f10x_rcc.h"
#include "stdlib.h"
#include "string.h"

typedef struct{
	u16* ADCdata;
	u32 i;
	u8 f;
}ADCstruct;

ADCstruct ADCstr;

void ADCInit();
void StartConversion();
void StopConversion();
void ResetData();
u16* GetData();

#endif
